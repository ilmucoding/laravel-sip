<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category; // panggil model Category

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::all();
        return view('category.index', compact('category'));
    }
    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->category_name = $request->category_name;
        $category->category_status = $request->category_status;
        $simpan = $category->save();
        if($simpan){
            return redirect('categories')->with('success', 'Berhasil menambah data kategori.');
        }
    }
    public function show($id)
    {
        $category = Category::where('category_id', $id)->first();
        return view('category.show', compact('category'));
    }
    public function edit($id)
    {
        $category = Category::where('category_id', $id)->first();
        return view('category.edit', compact('category'));
    }
    public function update(Request $request, $id)
    {
        $category = Category::where('category_id', $id)->first();
        $category->category_name = $request->category_name;
        $category->category_status = $request->category_status;
        $ubah = $category->save();
        if($ubah){
            return redirect('categories')->with('info', 'Berhasil mengubah data kategori.');
        }
    }
    public function destroy($id)
    {
        $category = Category::where('category_id', $id)->first();
        $hapus = $category->delete();
        if($hapus){
            return redirect('categories')->with('warning', 'Berhasil menghapus data kategori.');
        }
    }
}
