<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Transaction;

class DashboardController extends Controller
{
    public function index()
    {
        // $sql = "SELECT MONTHNAME(trx_date) month, count(*) total FROM transactions "."GROUP BY MONTHNAME(trx_date)"."ORDER BY MONTH(trx_date)";


        // $transactions = DB::select($sql);

        // $months = [];
        // $totals = [];

        // foreach ($transactions as $data) {
        //     $months[] = $data->month;
        //     $totals[] = $data->total;
        // }

        // $chart = [
        //     'months' => $months,
        //     'totals' => $totals
        // ];
        $paginate = 10;
        $latest_transactions = Transaction::orderBy('trx_date', 'desc')->paginate($paginate); 

        return view('dashboard', compact('latest_transactions', 'paginate'));
    }
}
