<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // ambil query string
        $cat_id = $request->query('cat_id');
        $keyword = $request->query('keyword');
        $categories = Category::pluck('category_name', 'category_id');

        $paginate = 1;
        $where = [];

        if(!empty($cat_id)) {
            $where[] = ['category_id', '=', $cat_id];
        }

        if(!empty($keyword)) {
            $where[] = ['product_name', 'LIKE', "%{$keyword}%"];
        }

        if(empty($cat_id) && empty($keyword)) {
            $products = Product::paginate($paginate);
        }
        else {
            $products = Product::where($where)->paginate($paginate);
        }

        $data = [
            'products' => $products,
            'categories' => $categories,
            'cat_id' => $cat_id,
            'keyword' => $keyword
        ];

        return view('product.index')->with($data);
    }

    public function create()
    {
        $categories = Category::where('category_status', 'Active')->pluck('category_name', 'category_id');
        return view('product.create', compact('categories'));
        // dd($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = new Product;
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_price = $request->product_price;
        $product->product_sku = $request->product_sku;
        $product->product_status = $request->product_status;
        $product->product_description = $request->product_description;

        if(!empty($request->file('product_image'))){
            $image = $request->file('product_image')->store('products', 'public');
            $product->product_image = $image;
        } else {
            $product->product_image = "products/no-image.png";
        }

        $simpan = $product->save();
        if($simpan){
            return redirect('products')->with('success', 'Berhasil menyimpan data produk');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::where('category_status', 'Active')->pluck('category_name', 'category_id');
        $product = Product::where('product_id', $id)->first();
        return view('product.show', compact('product', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('category_status', 'Active')->pluck('category_name', 'category_id');
        $product = Product::where('product_id', $id)->first();
        return view('product.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $product = Product::where('product_id', $id)->first();
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_price = $request->product_price;
        $product->product_sku = $request->product_sku;
        $product->product_status = $request->product_status;
        $product->product_description = $request->product_description;

        if(!empty($request->file('product_image'))){
            $image = $request->file('product_image')->store('products', 'public');
            $product->product_image = $image;
        }

        $ubah = $product->save();
        if($ubah){
            return redirect('products')->with('info', 'Berhasil mengubah data produk');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::where('product_id', $id)->first();
        $hapus = $product->delete();
        if($hapus){
            return redirect('products')->with('warning', 'Berhasil menghapus data produk.');
        }
    }
}
