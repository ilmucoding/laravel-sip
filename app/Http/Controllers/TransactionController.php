<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Transaction;

class TransactionController extends Controller
{
    public function index()
    {
        $paginate = 10;
        $transactions = Transaction::paginate($paginate);
        return view('transaction.index', compact('transactions', 'paginate'));
    }
    public function create()
    {
        return view('transaction.create');
    }
    public function import(Request $request)
    {
        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();

        if($data->count()){
            foreach($data as $key => $value){
                $arr[] = [
                    'product_id' => $value->product_id,
                    'trx_date' => $value->trx_date,
                    'trx_price' => $value->trx_price
                ];
            }
            if(!empty($arr)){
                Transaction::insert($arr);
            }
        }
        return redirect('transactions')->with('success', 'Berhasil mengimport data transaksi');
    }
    public function download()
    {
        $sql = "trx_id, trx_date, trx_price, product_name";
        $data = \App\Transaction::join('products', 'transactions.product_id', '=', 'products.product_id')
        ->selectRaw($sql)->get()->toArray();
        $tanggal = date('d-m-Y');
        return Excel::create('transaction_'.$tanggal, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }
}
