<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'product_name' => 'required|unique:products,product_name',
            'product_price' => 'required',
            'product_sku' => 'required',
            'product_status' => 'required',
            'product_description' => 'required',
            'product_image' => 'mimes:jpeg,jpg,png,gif|max:2000'
        ];
    }
    public function messages()
    {
        return [
            'category_id.required' => 'Kategori wajib diisi!',
            'product_name.required' => 'Name wajib diisi!',
            'product_name.unique' => 'Name produk tidak boleh sama dengan yang sebelumnya.',
            'product_price.required' => 'Price wajib diisi!',
            'product_sku.required' => 'SKU wajib diisi!',
            'product_status.required' => 'Status wajib diisi!',
            'product_description.required' => 'Description wajib diisi!',
            'product_image.mimes' => 'Gambar yang diupload hanya boleh bertipe jpg, png atau gif',
            'product_image.max' => 'Maksimal ukuran gambar adalah 2 Mb'
        ];
    }
}
