@extends('template')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Products</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        Edit Product
                    </div>
                    {{ Form::open(['url' => 'product/update/'.$product->product_id, 'files' =>  TRUE]) }}
                    <div class="card-body">
                    @if($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-hidden="close">x</a>
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('category', 'Category') }}
                                    {{ Form::select('category_id', $categories, $product->category_id, ['class' => 'form-control', 'placeholder' => 'Choose One']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_name', 'Name') }}
                                    {{ Form::text('product_name', $product->product_name, ['class' => 'form-control', 'placeholder' => 'Enter product name']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_price', 'Price') }}
                                    {{ Form::text('product_price', $product->product_price, ['class' => 'form-control', 'placeholder' => 'Enter product price']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('product_sku', 'SKU') }}
                                    {{ Form::text('product_sku', $product->product_sku, ['class' => 'form-control', 'placeholder' => 'Enter product sku']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_status', 'Status') }}
                                    {{ Form::select('product_status', ['Active' => 'Active', 'Inactive' => 'Inactive'], $product->product_status, ['class' => 'form-control', 'placeholder' => 'Choose One']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_image', 'Image') }}
                                    <br>
                                    <img src="{{ asset('storage/'.$product->product_image) }}" alt="" width="150">
                                    <br>
                                    {{ Form::label('product_image', 'Ganti Image') }}
                                    {{ Form::file('product_image', ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('product_description', 'Description') }}
                                    {{ Form::textarea('product_description', $product->product_description, ['class' => 'form-control', 'placeholder' => 'Enter product description', 'rows' => 3]) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url('products') }}" class="btn btn-outline-info">Back</a>
                        <button type="submit" class="btn btn-primary float-right">Update</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection