@extends('template')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Categories</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Categories</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        List Category
                        <a href="{{ url('product/create') }}" class="btn btn-primary btn-sm float-right">Tambah</a>
                    </div>
                    <div class="card-body">
                        <?php
                            if($msg_success = Session::get('success')){
                                $class = "alert alert-success alert-dismissable";
                                $msg = $msg_success;
                            } else if($msg_info = Session::get('info')){
                                $class = "alert alert-info alert-dismissable";
                                $msg = $msg_info;
                            } else if($msg_warning = Session::get('warning')){
                                $class = "alert alert-warning alert-dismissable";
                                $msg = $msg_warning;
                            } else {
                                $class = "d-none";
                                $msg = "";
                            }
                        ?>
                        <div class="{{ $class }}" id="alert-msg">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            {{ $msg }}
                        </div>
                        <div class="row mt-3 mb-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('category', 'Category') }}
                                    {{ Form::select('category', $categories, $cat_id, ['class' => 'form-control', 'placeholder' => 'Choose One']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('search', 'Search') }}
                                    {{ Form::text('search', $keyword, ['class' => 'form-control', 'placeholder' => 'Enter keyword']) }}
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Category</td>
                                        <td>Name</td>
                                        <td>Price</td>
                                        <td>SKU</td>
                                        <td>Status</td>
                                        <td>Image</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $key => $data)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $data->category->category_name }}</td>
                                        <td>{{ $data->product_name }}</td>
                                        <td>{{ $data->product_price }}</td>
                                        <td>{{ $data->product_sku }}</td>
                                        <td>{{ $data->product_status }}</td>
                                        <td><img src="{{ asset('storage/'.$data->product_image) }}" width="100"></td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ url('product/show/'.$data->product_id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                                <a href="{{ url('product/edit/'.$data->product_id) }}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                                <a href="{{ url('product/delete/'.$data->product_id) }}" class="btn btn-danger" onclick="return confirm('Apakah Anda ingin menghapus produk {{ $data->product_name }} ini?')"><i class="fa fa-trash-alt"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row float-right">
                            <div class="col-md-12">
                                {{ $products->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>

$(document).ready(function() {
    // show the alert
    setTimeout(function() {
      $("#alert-msg").slideUp(500);
    }, 2000);

    $("#category").on('change', function() {
        filter();
    });

    $("#search").keypress(function(event){
        if(event.keyCode == 13) { // kode enter 
            filter();
        }
    });

    var filter = function(){
        var catId = $("#category").val();
        var keyword = $("#search").val();

        window.location.replace("{{ url('products') }}?cat_id=" + catId + "&keyword=" + keyword);
    }
});

</script>
@endsection