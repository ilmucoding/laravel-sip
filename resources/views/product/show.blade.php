@extends('template')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Products</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        Detail Product
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('category', 'Category') }}
                                    {{ Form::text('category_id', $product->category->category_name, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_name', 'Name') }}
                                    {{ Form::text('product_name', $product->product_name, ['class' => 'form-control', 'placeholder' => 'Enter product name', 'disabled' => 'disabled']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_price', 'Price') }}
                                    {{ Form::text('product_price', $product->product_price, ['class' => 'form-control', 'placeholder' => 'Enter product price', 'disabled' => 'disabled']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('product_sku', 'SKU') }}
                                    {{ Form::text('product_sku', $product->product_sku, ['class' => 'form-control', 'placeholder' => 'Enter product sku', 'disabled' => 'disabled']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_status', 'Status') }}
                                    {{ Form::text('product_status', $product->product_status, ['class' => 'form-control', 'placeholder' => 'Choose One', 'disabled' => 'disabled']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('product_image', 'Image') }}
                                    <br>
                                    <img src="{{ asset('storage/'.$product->product_image) }}" alt="" width="150">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('product_description', 'Description') }}
                                    {{ Form::textarea('product_description', $product->product_description, ['class' => 'form-control', 'placeholder' => 'Enter product description', 'rows' => 3, 'disabled' => 'disabled']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url('products') }}" class="btn btn-outline-info">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection