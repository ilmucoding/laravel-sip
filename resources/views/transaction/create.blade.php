@extends('template')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Transactions</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Transactions</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        Import Transaction
                    </div>
                    {{ Form::open(['url' => 'transaction/import', 'files' => TRUE]) }}
                    <div class="card-body">
                        
                        <div class="alert alert-info">
                            <p>Silahkan download format import file di <a href="{{ asset('storage/files/data_trx.xlsx') }}">sini</a></p>
                        </div>

                        <div class="form-group">
                            {{ Form::label('import', 'File') }}
                            {{ Form::file('import_file', ['class' => 'form-control']) }}
                        </div>

                    </div>
                    <div class="card-footer">
                        <a href="{{ url('transactions') }}" class="btn btn-outline-info">Back</a>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection