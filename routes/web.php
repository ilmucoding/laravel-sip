<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login')->middleware('admin');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login')->middleware('admin');
Route::post('login', 'Auth\LoginController@login');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('/admin/home', 'HomeController@index');

Route::group(['middleware' => ['auth']], function() {

    Route::get('/home', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index');
    // Module Category
    Route::get('/categories', 'CategoryController@index');
    Route::get('/category/create', 'CategoryController@create');
    Route::post('/category/store', 'CategoryController@store');
    Route::get('/category/edit/{id}', 'CategoryController@edit');
    Route::post('/category/update/{id}', 'CategoryController@update');
    Route::get('/category/show/{id}', 'CategoryController@show');
    Route::get('/category/delete/{id}', 'CategoryController@destroy');

    // Module Product
    Route::get('/products', 'ProductController@index');
    Route::get('/product/create', 'ProductController@create');
    Route::post('/product/store', 'ProductController@store');
    Route::get('/product/edit/{id}', 'ProductController@edit');
    Route::post('/product/update/{id}', 'ProductController@update');
    Route::get('/product/show/{id}', 'ProductController@show');
    Route::get('/product/delete/{id}', 'ProductController@destroy');

    // Module Transaction
    Route::get('/transactions', 'TransactionController@index');
    Route::get('/transaction/create', 'TransactionController@create');
    Route::post('/transaction/import', 'TransactionController@import');
    Route::get('/transaction/download', 'TransactionController@download');

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

});